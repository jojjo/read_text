import 'dart:math';

import 'package:flutter/material.dart';
import 'package:read_text/actionable_string.dart';
import 'package:read_text/bloc/bloc_provider.dart';
import 'package:read_text/bloc/ocr_bloc.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_selectable_text/flutter_selectable_text.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '4ME306 Prototype',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: BlocProvider<OcrBloc>(
        bloc: OcrBloc(),
        child: OcrPage(),
      ),
    );
  }
}

class OcrPage extends StatelessWidget {

  var intro = Text("Take a photo of the text that you want processed by pressing the camera button below.", textAlign: TextAlign.center, textScaleFactor: 1.5,);
  var resultPhone = Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text("The following phone numbers were detected, press the phone number for available actions", textAlign: TextAlign.center, textScaleFactor: 1.5,));
  var resultLink = Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text("The following links were detected, press the link for available actions", textAlign: TextAlign.center, textScaleFactor: 1.5,));

  Padding resultText = Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text("The following text was detected, press to copy or select:", textAlign: TextAlign.center, textScaleFactor: 1.5,));

  @override
  Widget build(BuildContext context) {
    final OcrBloc bloc = BlocProvider.of<OcrBloc>(context);

    return Scaffold(
      appBar: AppBar(title: Text('Text convertion prototype app')),
      body: StreamBuilder<List<ActionableString>>(
          stream: bloc.processedText,
          builder:
              (BuildContext context, AsyncSnapshot<List<ActionableString>> snapshot) {
              var widgets = <Widget>[];
              widgets.add(resultText);
            if (snapshot.hasData) {
              for (var actionableString in snapshot.data) {
                if (actionableString.type == ActionableStringType.plain) {
                  widgets.add(SelectableText(actionableString.text, textAlign: TextAlign.center,));
                } else if (actionableString.type == ActionableStringType.telephone) {
                  widgets.add(resultPhone);
                  widgets.add(FlatButton(
                      onPressed: () => launch("tel://${actionableString.text}"),
                      child: Text(actionableString.text)));
                } else if (actionableString.type == ActionableStringType.link) {
                  widgets.add(resultLink);
                  widgets.add(FlatButton(
                      onPressed: () => launch("${actionableString.text}"),
                      child: Text(actionableString.text)));
                }
              }
              return ListView(
                  padding: const EdgeInsets.all(8.0),
                  children: widgets);
            }
            return Container(child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(child: intro),
            ));
          }),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.photo_camera),
        onPressed: () {
          bloc.selectImage.add(null);
        },
      ),
    );
  }
}
