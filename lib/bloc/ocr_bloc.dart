import 'dart:async';

import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:image_picker/image_picker.dart';
import 'package:read_text/actionable_string.dart';
import 'package:read_text/bloc/bloc_provider.dart';
import 'package:rxdart/rxdart.dart';

class OcrBloc implements BlocBase {
  String text;
  FirebaseVisionImage visionImage;
  final TextRecognizer textRecognizer =
      FirebaseVision.instance.textRecognizer();

  var _ocrController = PublishSubject<List<ActionableString>>();
  StreamSink<List<ActionableString>> get _inText => _ocrController.sink;
  Stream<List<ActionableString>> get processedText => _ocrController.stream;

  StreamController _actionController = StreamController();
  StreamSink get selectImage => _actionController.sink;

  OcrBloc() {
    _actionController.stream.listen(_handleLogic);
  }

  void dispose() {
    _actionController.close();
    _ocrController.close();
  }

  void _handleLogic(data) {
    _inText.add(List());
    Observable.fromFuture(ImagePicker.pickImage(source: ImageSource.camera))
        .listen((imageFile) {
      visionImage = FirebaseVisionImage.fromFile(imageFile);
      Observable.fromFuture(textRecognizer.processImage(visionImage))
          .listen((visionText) {
        var list = findText(visionText)
          ..addAll(findPhoneNumbers(visionText))
          ..addAll(findLinks(visionText));
        _inText.add(list);
      });
    });
  }

  List<ActionableString> findLinks(VisionText visionText) {
    var regex = RegExp(
        r'(https?:\/\/)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)');
    var blocks = visionText.blocks;
    var linkCount = 0;
    List<ActionableString> list = List();
    for (var block in blocks) {
      for (var line in block.lines) {
        Iterable<Match> matches = regex.allMatches(line.text);
        if (matches.length > 0) {
          for (Match m in matches) {
            String match = m.group(0);
            if (match.contains("http://") || match.contains("https://")) {
              list.add(LinkString()..setText(match));
            } else {
              list.add(LinkString()..setText('http://$match'));
            }
          }
        }
      }
    }
    return list;
  }

  List<ActionableString> findPhoneNumbers(VisionText visionText) {
    var regex = RegExp(r'(?!\s)([\s\-\+0-9]{7,})\b');
    var blocks = visionText.blocks;
    var telCount = 0;
    List<ActionableString> list = List();
    for (var block in blocks) {
      for (var line in block.lines) {
        print(line.text);
        var temp = line.text;
        Iterable<Match> matches = regex.allMatches(line.text);
        if (matches.length > 0) {
          for (Match m in matches) {
            String match = m.group(0).trim();
            if (match.length > 6) {
              list.add(TelephoneString()..setText(match));
              print(match);
            }
          }
        }
      }
    }
    return list;
  }

  List<ActionableString> findText(VisionText visionText) {
    var blocks = visionText.blocks;
    List<ActionableString> list = List();
    for (var block in blocks) {
      for (var line in block.lines) {
        list.add(PlainOldString()..setText(line.text));
      }
    }
    return list;
  }
}
