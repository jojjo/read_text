enum ActionableStringType { plain, telephone, link }

abstract class ActionableString {
  var _text;
  var type;
  String get text => _text;
  void setText(text) => _text = text;
}

class PlainOldString extends ActionableString {
  @override
  var type = ActionableStringType.plain;
}
class TelephoneString extends ActionableString {
  @override
  var type = ActionableStringType.telephone;
}

class LinkString extends ActionableString {
  @override
  var type = ActionableStringType.link;
}

